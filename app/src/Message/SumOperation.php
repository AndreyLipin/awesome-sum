<?php


namespace App\Message;


use App\Entity\SumResult;

class SumOperation
{
    private int $operand1;
    private int $operand2;

    public function __construct(SumResult $data)
    {
        $this->operand1 = $data->getOperator1();
        $this->operand2 = $data->getOperator2();
    }

    public function getOperand1(): int
    {
        return $this->operand1;
    }

    public function getOperand2(): int
    {
        return $this->operand2;
    }
}