<?php

namespace App\Entity;

use App\Repository\SumResultRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SumResultRepository::class)
 */
class SumResult
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="integer")
     */
    private int $operator1;

    /**
     * @ORM\Column(type="integer")
     */
    private int $operator2;

    /**
     * @ORM\Column(type="integer")
     */
    private int $result;

    public function getId(): int
    {
        return $this->id;
    }

    public function getOperator1(): int
    {
        return $this->operator1;
    }

    public function setOperator1(int $operator1): self
    {
        $this->operator1 = $operator1;

        return $this;
    }

    public function getOperator2(): int
    {
        return $this->operator2;
    }

    public function setOperator2(int $operator2): self
    {
        $this->operator2 = $operator2;

        return $this;
    }

    public function getResult(): int
    {
        return $this->result;
    }

    public function setResult(int $result): self
    {
        $this->result = $result;

        return $this;
    }
}
