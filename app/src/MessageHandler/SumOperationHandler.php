<?php


namespace App\MessageHandler;

use App\Entity\SumResult;
use App\Message\SumOperation;
use App\Repository\SumResultRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class SumOperationHandler implements MessageHandlerInterface
{
    private SumResultRepository $sumResultRepository;
    private EntityManagerInterface $em;
    private LoggerInterface $logger;

    public function __construct(SumResultRepository $sumResultRepository, EntityManagerInterface $em)
    {
        $this->sumResultRepository = $sumResultRepository;
        $this->em = $em;
    }

    public function __invoke(SumOperation $message)
    {
        $sumResult = new SumResult();
        $sumResult->setOperator1($message->getOperand1());
        $sumResult->setOperator2($message->getOperand2());
        $sumResult->setResult($this->sum($sumResult->getOperator1(), $sumResult->getOperator2()));

        $this->save($sumResult);
    }

    private function sum($operator1, $operator2): int
    {
        return $operator1 + $operator2;
    }

    private function save(SumResult $sumResult): void
    {
        $this->em->persist($sumResult);
        $this->em->flush();
    }
}