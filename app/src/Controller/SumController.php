<?php

namespace App\Controller;

use App\Entity\SumResult;
use App\Form\SumResultType;
use App\Message\SumOperation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class SumController extends AbstractController
{
    /**
     * @Route("/sum", name="sum")
     * @param Request $request
     * @param MessageBusInterface $bus
     * @return Response
     */
    public function index(Request $request, MessageBusInterface $bus): Response
    {
        $sumResult = new SumResult();
        $form = $this->createForm(SumResultType::class, $sumResult);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $bus->dispatch(new SumOperation($form->getData()));
        }
        return $this->render('sum/index.html.twig', ['form' => $form->createView()]);
    }
}
